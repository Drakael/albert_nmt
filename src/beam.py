import torch
from src.model import nopeak_mask
import torch.nn.functional as F
import math
from time import time


def init_vars(src, model, args):
    init_tok = args.sos_id
    src_mask = (src != args.pad_id).unsqueeze(-2)
    e_output = model.encoder(src, src_mask)

    outputs = torch.LongTensor([[init_tok]])
    if args.device == 0:
        outputs = outputs.cuda()

    trg_mask = nopeak_mask(1, args)

    out = model.out(model.decoder(outputs,
                    e_output, src_mask, trg_mask))
    out = F.softmax(out, dim=-1)

    probs, ix = out[:, -1].data.topk(args.k)
    log_scores = torch.Tensor([math.log(p)
                               for p in probs.data[0]]).unsqueeze(0)

    outputs = torch.zeros(args.k, args.max_tokens).long()
    if args.device == 0:
        outputs = outputs.cuda()
    outputs[:, 0] = init_tok
    outputs[:, 1] = ix[0]

    e_outputs = torch.zeros(args.k, e_output.size(-2), e_output.size(-1))
    if args.device == 0:
        e_outputs = e_outputs.cuda()
    e_outputs[:, :] = e_output[0]

    return outputs, e_outputs, log_scores


def k_best_outputs(outputs, out, log_scores, i, k):
    probs, ix = out[:, -1].data.topk(k)
    log_probs = torch.Tensor([math.log(p) for p in probs.data.view(-1)]).view(k, -1) + log_scores.transpose(0, 1)
    k_probs, k_ix = log_probs.view(-1).topk(k)

    row = k_ix // k
    col = k_ix % k

    outputs[:, :i] = outputs[row, :i]
    outputs[:, i] = ix[row, col]

    log_scores = k_probs.unsqueeze(0)

    return outputs, log_scores


def beam_search(src, model, args):
    t0 = time()
    outputs, e_outputs, log_scores = init_vars(src, model, args)
    eos_tok = args.eos_id
    src_mask = (src != args.pad_id).unsqueeze(-2)
    ind = None
    for i in range(1, args.max_tokens):
        trg_mask = nopeak_mask(i, args)

        out = model.out(model.decoder(outputs[:, :i],
                                      e_outputs, src_mask, trg_mask))

        out = F.softmax(out, dim=-1)

        outputs, log_scores = k_best_outputs(outputs, out, log_scores, i, args.k)

        ones = (outputs == eos_tok).nonzero()  # Occurrences of end symbols for all input sentences.
        sentence_lengths = torch.zeros(len(outputs), dtype=torch.long).cuda()
        for vec in ones:
            i = vec[0]
            if sentence_lengths[i] == 0:  # First end symbol has not been found yet
                sentence_lengths[i] = vec[1]  # Position of first end symbol

        num_finished_sentences = len([s for s in sentence_lengths if s > 0])

        if num_finished_sentences == args.k:
            alpha = 0.7
            div = 1/(sentence_lengths.type_as(log_scores)**alpha)
            _, ind = torch.max(log_scores * div, 1)
            ind = ind.data[0]
            break
    if ind is not None:
        ind = int(ind.cpu().numpy())
    outputs = outputs.cpu().numpy()
    print('done in ', str(time() - t0))
    if ind is None:
        eos_tokens = (outputs[0] == eos_tok).nonzero()[0].tolist()
        if type(eos_tokens) is list and len(eos_tokens) == 0:
            length = len(outputs[0])
        elif type(eos_tokens) is list:
            length = int(eos_tokens[0])
        else:
            length = int(eos_tokens)
        return ' '.join([args.trg_id2t[str(tok)] for tok in outputs[0][1:length]])

    else:
        eos_tokens = (outputs[ind] == eos_tok).nonzero()[0].tolist()
        if type(eos_tokens) is list and len(eos_tokens) == 0:
            length = len(outputs[ind])
        elif type(eos_tokens) is list:
            length = int(eos_tokens[0])
        else:
            length = int(eos_tokens)
        return ' '.join([args.trg_id2t[str(tok)] for tok in outputs[ind][1:length]])
