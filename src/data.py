# coding: utf-8
import numpy as np
from torch import from_numpy
import re
import json
import os


def tokenize_en(text):
    text = text.replace('a.m.', 'AM').replace('"', ' " ')
    text = text.replace('.', ' .').replace('!', ' !')
    text = text.replace('?', ' .').replace("'", " '")
    text = text.replace(',', ' , ').replace("’", " '")
    text = re.sub(r"\s+", " ", text)
    return text.split()


def tokenize_fr(text):
    text = text.replace("'", "' ").replace('’', "' ")
    text = text.replace("-t-", " -t- ").replace("-", " - ")
    text = text.replace(',', ' , ').replace('.', ' . ')
    text = text.replace('?', ' ? ').replace('!', ' ! ')
    text = text.replace('«', ' " ').replace('»', ' " ')
    text = text.replace('"', ' " ')
    text = re.sub(r"\s+", " ", text)
    return text.split()


def process_data(args, encoding='utf8'):
    with open(args.src, 'r', encoding=encoding) as f:
        args.src_str_list = f.read().strip().split('\n')
    with open(args.trg, 'r', encoding=encoding) as f:
        args.trg_str_list = f.read().strip().split('\n')
    assert len(args.src_str_list) == len(args.trg_str_list), \
        "Files must contain the same number of lines"
    args.train_len = len(args.src_str_list)
    args.batch_count = args.train_len / args.batch_size
    print('Number of examples :', len(args.src_str_list))

    args.src_tokens = [tokenize_en(t) for t in args.src_str_list]
    args.trg_tokens = [tokenize_fr(t) for t in args.trg_str_list]

    args.pad_id = 0
    args.unk_id = 1
    args.sos_id = 2
    args.eos_id = 3
    args.mask_id = 4
    vocab_path = os.path.join('data', 'vocab.json')
    if os.path.exists(vocab_path):
        with open(vocab_path, 'r') as f:
            vocab = json.load(f)
            args.src_t2id = vocab['src_t2id']
            args.src_id2t = vocab['src_id2t']
            args.trg_t2id = vocab['trg_t2id']
            args.trg_id2t = vocab['trg_id2t']
    else:
        args.src_t2id = {}
        args.src_t2id['<pad>'] = args.pad_id
        args.src_t2id['<unk>'] = args.unk_id
        args.src_t2id['<sos>'] = args.sos_id
        args.src_t2id['<eos>'] = args.eos_id
        args.src_t2id['<mask>'] = args.mask_id
        cnt = 5
        for seq in args.src_tokens:
            for t in seq:
                if t not in args.src_t2id.keys():
                    args.src_t2id[t] = cnt
                    cnt += 1
        args.src_id2t = {v: k for k, v in args.src_t2id.items()}
        args.trg_t2id = {}
        args.trg_t2id['<pad>'] = args.pad_id
        args.trg_t2id['<unk>'] = args.unk_id
        args.trg_t2id['<sos>'] = args.sos_id
        args.trg_t2id['<eos>'] = args.eos_id
        args.trg_t2id['<mask>'] = args.mask_id
        cnt = 5
        for seq in args.trg_tokens:
            for t in seq:
                if t not in args.trg_t2id.keys():
                    args.trg_t2id[t] = cnt
                    cnt += 1
        args.trg_id2t = {v: k for k, v in args.trg_t2id.items()}
        vocab = {}
        vocab['src_t2id'] = args.src_t2id
        vocab['src_id2t'] = args.src_id2t
        vocab['trg_t2id'] = args.trg_t2id
        vocab['trg_id2t'] = args.trg_id2t
        with open(vocab_path, 'w') as f:
            json.dump(vocab, f)
    args.src_vocab_size = len(args.src_t2id.keys())
    args.trg_vocab_size = len(args.trg_t2id.keys())
    print('Src vocab size :', args.src_vocab_size)
    print('Trg vocab size :', args.trg_vocab_size)

    args.src_ids_list = np.array([[args.src_t2id[t] for t in s]
                                  for s in args.src_tokens])
    args.trg_ids_list = [[args.trg_t2id[t] for t in s]
                         for s in args.trg_tokens]
    # we add <sos> and <eos> tokens at the begining and end of  target sequence
    args.trg_ids_list = np.array([[args.sos_id] + s + [args.eos_id]
                                  for s in args.trg_ids_list])

    src_counts = np.array([len(s) for s in args.src_ids_list])
    trg_counts = np.array([len(s) for s in args.trg_ids_list])

    mean_counts = np.vstack([src_counts, trg_counts]).mean(axis=0)
    mean_counts = trg_counts

    args.src_max = np.max(src_counts)
    args.src_min = np.min(src_counts)
    args.trg_max = np.max(trg_counts)
    args.trg_min = np.min(trg_counts)
    print('Max source sequence length :', args.src_max)
    print('Min source sequence length :', args.src_min)
    print('Max target sequence length (with <sos> and <eos> tokens):',
          args.trg_max)
    print('Min target sequence length (with <sos> and <eos> tokens) :',
          args.trg_min)
    args.max_tokens = max(args.src_max, args.trg_max)
    args.src_mat = np.full((args.train_len, args.src_max), args.pad_id)
    args.trg_mat = np.full((args.train_len, args.trg_max), args.pad_id)
    # order samples from longest source length to shortest by mean
    mean_sort_ids_desc = np.argsort(mean_counts)[::-1]

    for i in range(args.train_len):
        id_ = mean_sort_ids_desc[i]
        args.src_mat[i, :len(args.src_ids_list[id_])] = args.src_ids_list[id_]
        args.trg_mat[i, :len(args.trg_ids_list[id_])] = args.trg_ids_list[id_]

    src_str_temp_list = [args.src_str_list[i] for i in mean_sort_ids_desc]
    args.src_str_list = src_str_temp_list
    trg_str_temp_list = [args.trg_str_list[i] for i in mean_sort_ids_desc]
    args.trg_str_list = trg_str_temp_list

    src_counts_temp_list = [src_counts[i] for i in mean_sort_ids_desc]
    src_counts = src_counts_temp_list
    trg_counts_temp_list = [trg_counts[i] for i in mean_sort_ids_desc]
    trg_counts = trg_counts_temp_list

    src_ids_temp_list = [args.src_ids_list[i] for i in mean_sort_ids_desc]
    args.src_ids_list = np.array(src_ids_temp_list)
    trg_ids_temp_list = [args.trg_ids_list[i] for i in mean_sort_ids_desc]
    args.trg_ids_list = np.array(trg_ids_temp_list)

    return DataIterator(args.src_mat, args.trg_mat,
                        src_counts, trg_counts,
                        args.batch_size, args.src_vocab_size,
                        args.trg_vocab_size, args.device)


class DataIterator:
    def __init__(self, src_mat, trg_mat, src_counts, trg_counts,
                 batch_size, src_vocab_size, trg_vocab_size, device):
        self.src_mat = src_mat
        self.trg_mat = trg_mat
        self.src_counts = src_counts
        self.trg_counts = trg_counts
        self.batch_size = batch_size
        self.src_vocab_size = src_vocab_size
        self.trg_vocab_size = trg_vocab_size
        self.device = device
        self.examples_count = src_mat.shape[0]
        self.batch_count = self.examples_count / self.batch_size
        self.current = -1
        self.processed = 0

    def __iter__(self):
        return self

    def __next__(self):
        self.current += 1
        if self.current < self.batch_count:
            # print('self.current % 2 ==', self.current % 2)
            if self.current % 2 == 0:
                start = (self.current // 2) * self.batch_size
            else:
                start = - ((self.current + 1) // 2) * self.batch_size
            # last batch is probably not full
            batch_size = min(self.examples_count - self.processed, self.batch_size)
            end = start + batch_size
            if self.current % 2 == 1 and end == 0:
                end = None
            src_max_len = np.max(self.src_counts[start:end])
            trg_max_len = np.max(self.trg_counts[start:end])
            batch_mat_src = from_numpy(self.src_mat[start:end, :src_max_len]).long()
            batch_mat_trg = from_numpy(self.trg_mat[start:end, :trg_max_len]).long()
            self.processed += batch_size
            # print('start', start, 'end', end)
            # print('batch_mat_src.size()', batch_mat_src.size())
            # print('batch_mat_trg.size()', batch_mat_trg.size())
            # batch_mat_src_onehot = one_hot(batch_mat_src, num_classes=self.vocab_size)
            # print('batch_mat_src_onehot.size()', batch_mat_src_onehot.size())
            # batch_mat_trg_onehot = one_hot(batch_mat_trg, num_classes=self.vocab_size)
            # print('batch_mat_trg_onehot.size()', batch_mat_trg_onehot.size())
            return (batch_mat_src.to(self.device),
                    batch_mat_trg.to(self.device))
            # return (batch_mat_src_onehot.to(self.device),
            #         batch_mat_trg_onehot.to(self.device))
        else:
            self.current = -1
            self.processed = 0
            raise StopIteration
