# coding: utf-8
import torch.nn as nn
from src.sublayers import FeedForward, MultiHeadAttention, Norm


class EncoderLayer(nn.Module):
    def __init__(self, attn, ff, d_hidden, heads, device, dropout=0.1):
        super().__init__()
        self.norm_1 = Norm(d_hidden, device)
        self.norm_2 = Norm(d_hidden, device)
        self.attn = attn
        self.ff = ff
        self.dropout_1 = nn.Dropout(dropout)
        self.dropout_2 = nn.Dropout(dropout)

    def forward(self, x, mask):
        x2 = self.norm_1(x)
        x = x + self.dropout_1(self.attn(x2, x2, x2, mask))
        x2 = self.norm_2(x)
        x = x + self.dropout_2(self.ff(x2))
        return x


# build a decoder layer with two multi-head attention layers and
# one feed-forward layer
class DecoderLayer(nn.Module):
    def __init__(self, attn_1, attn_2, ff, d_hidden, heads, device, dropout=0.1):
        super().__init__()
        self.norm_1 = Norm(d_hidden, device)
        self.norm_2 = Norm(d_hidden, device)
        self.norm_3 = Norm(d_hidden, device)

        self.dropout_1 = nn.Dropout(dropout)
        self.dropout_2 = nn.Dropout(dropout)
        self.dropout_3 = nn.Dropout(dropout)

        self.attn_1 = attn_1
        self.attn_2 = attn_2
        self.ff = ff

    def forward(self, x, e_outputs, src_mask, trg_mask):
        x2 = self.norm_1(x)
        x = x + self.dropout_1(self.attn_1(x2, x2, x2, trg_mask))
        x2 = self.norm_2(x).long()
        x = x + self.dropout_2(self.attn_2(x2, e_outputs, e_outputs,
                               src_mask))
        x2 = self.norm_3(x)
        x = x + self.dropout_3(self.ff(x2))
        return x
