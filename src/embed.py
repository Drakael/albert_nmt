import torch
import torch.nn as nn
import math
from torch.autograd import Variable
from src.sublayers import Norm


class Embedder(nn.Module):
    def __init__(self, vocab_size, d_vocab, d_hidden, max_tokens, device):
        super().__init__()
        # factorized embedding
        self.vocab_embed = nn.Embedding(vocab_size, d_vocab)
        self.hidden_embed = nn.Linear(d_vocab, d_hidden)
        # self.pos_embed = nn.Embedding(max_tokens, d_hidden) # position embedding
        # self.seg_embed = nn.Embedding(cfg.n_segments, cfg.hidden) # segment(token type) embedding
        self.norm = Norm(d_hidden, device)

    def forward(self, x):
        seq_len = x.size(1)
        # pos = torch.arange(seq_len, dtype=torch.long, device=x.device)
        # pos = pos.unsqueeze(0).expand_as(x) # (S,) -> (B, S)
        # factorized embedding
        e = self.vocab_embed(x)
        e = self.hidden_embed(e)
        # e = e + self.pos_embed(pos) # + self.seg_embed(seg)
        return self.norm(e)


class PositionalEncoder(nn.Module):
    def __init__(self, d_hidden, max_seq_len=150, dropout=0.1):
        super().__init__()
        self.d_hidden = d_hidden
        self.dropout = nn.Dropout(dropout)
        # create constant 'pe' matrix with values dependant on
        # pos and i
        pe = torch.zeros(max_seq_len, d_hidden, dtype=torch.float32)
        pe.require_grad = False
        position = torch.arange(0, max_seq_len, dtype=torch.float32).unsqueeze(1)
        div_term = (torch.arange(0, d_hidden, 2).float() * -(math.log(10000.0) / d_hidden)).exp()
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0)
        self.register_buffer('pe', pe)

    def forward(self, x):
        # make embeddings relatively larger
        x = x * math.sqrt(self.d_hidden)
        # add constant to embedding
        seq_len = x.size(1)
        pe = Variable(self.pe[:, :seq_len], requires_grad=False)
        # pe dimensions are 1 * seq_len * d_hidden
        if x.is_cuda:
            pe.cuda()
        x = x + pe
        return self.dropout(x)
