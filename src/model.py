# coding: utf-8
import torch
import torch.nn as nn
import numpy as np
from torch.autograd import Variable
from src.layers import EncoderLayer, DecoderLayer
from src.embed import Embedder, PositionalEncoder
from src.sublayers import FeedForward, MultiHeadAttention, Norm
import os


class Encoder(nn.Module):
    def __init__(self, vocab_size, d_vocab, d_hidden, N, heads,
                 dropout, max_seq_len, device):
        super().__init__()
        self.N = N
        self.embed = Embedder(vocab_size, d_vocab, d_hidden, max_seq_len, device)
        self.pe = PositionalEncoder(d_hidden, dropout=dropout,
                                    max_seq_len=max_seq_len)
        # self.layer = EncoderLayer(d_hidden, heads, dropout)
        self.attn = MultiHeadAttention(heads, d_hidden, dropout=dropout)
        self.ff = FeedForward(d_hidden, dropout=dropout)
        self.layers = []
        for i in range(self.N):
            self.layers.append(EncoderLayer(self.attn, self.ff, d_hidden, heads, device, dropout))
        self.norm = Norm(d_hidden, device)

    def forward(self, src, mask):
        x = self.embed(src)
        x = self.pe(x)
        for i in range(self.N):
            x = self.layers[i](x, mask)
        return self.norm(x)


class Decoder(nn.Module):
    def __init__(self, vocab_size, d_vocab, d_hidden, N, heads,
                 dropout, max_seq_len, device):
        super().__init__()
        self.N = N
        self.embed = Embedder(vocab_size, d_vocab, d_hidden, max_seq_len, device)
        self.pe = PositionalEncoder(d_hidden, dropout=dropout,
                                    max_seq_len=max_seq_len)
        # self.layer = DecoderLayer(d_hidden, heads, dropout)
        self.attn_1 = MultiHeadAttention(heads, d_hidden, dropout=dropout)
        self.attn_2 = MultiHeadAttention(heads, d_hidden, dropout=dropout)
        self.ff = FeedForward(d_hidden, dropout=dropout)
        self.layers = []
        for i in range(self.N):
            self.layers.append(DecoderLayer(self.attn_1, self.attn_2, self.ff, d_hidden, heads, device, dropout))
        self.norm = Norm(d_hidden, device)

    def forward(self, trg, e_outputs, src_mask, trg_mask):
        x = self.embed(trg)
        x = self.pe(x)
        for i in range(self.N):
            x = self.layers[i](x, e_outputs, src_mask, trg_mask)
        return self.norm(x)


class Transformer(nn.Module):
    def __init__(self, src_vocab_size, trg_vocab_size, d_vocab, d_hidden,
                 N, heads, dropout, max_seq_len, device):
        super().__init__()
        self.encoder = Encoder(src_vocab_size, d_vocab, d_hidden, N, heads,
                               dropout, max_seq_len, device)
        self.decoder = Decoder(trg_vocab_size, d_vocab, d_hidden, N, heads,
                               dropout, max_seq_len, device)
        self.out = nn.Linear(d_hidden, trg_vocab_size)

    def forward(self, src, trg, src_mask, trg_mask):
        e_outputs = self.encoder(src, src_mask)
        d_output = self.decoder(trg, e_outputs, src_mask, trg_mask)
        output = self.out(d_output)
        return output


def get_model(args):
    assert args.d_hidden % args.heads == 0
    assert args.dropout < 1

    model = Transformer(args.src_vocab_size, args.trg_vocab_size,
                        args.d_vocab, args.d_hidden, args.n_layers,
                        args.heads, args.dropout, args.max_tokens,
                        args.device)

    model_file = args.ckpt_file or 'model_weights.ckpt'
    model_path = os.path.join(args.load_weights, model_file)
    if args.load_weights is not None \
            and os.path.exists(model_path):
        print("loading pretrained weights...")
        model.load_state_dict(torch.load(model_path))
    else:
        for p in model.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)
    if args.device == 0:
        model = model.cuda()
    return model


def nopeak_mask(size, args):
    np_mask = np.triu(np.ones((1, size, size)),
                      k=1).astype('uint8')
    np_mask = Variable(torch.from_numpy(np_mask) == 0)
    # np_mask.size = 1 * seq_len+1 * seq_len+1
    if args.device == 0:
        np_mask = np_mask.cuda()
    return np_mask


def create_masks(src, trg, args):
    src_mask = (src != args.pad_id).unsqueeze(-2)
    if trg is not None:
        trg_mask = (trg != args.pad_id).unsqueeze(-2)
        size = trg.size(1)  # get seq_len for matrix
        np_mask = nopeak_mask(size, args)
        if trg.is_cuda:
            np_mask.cuda()
        trg_mask = trg_mask & np_mask
    else:
        trg_mask = None
    # src_mask & trg_mask dimensions are batch_size * 1 * seq_len
    return src_mask, trg_mask
