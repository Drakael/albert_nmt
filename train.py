# coding: utf-8
import argparse
from src.data import process_data
from src.model import get_model
from src.trainer import train_model
import torch


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-src', default='data/english.txt')
    parser.add_argument('-trg', default='data/french.txt')
    # parser.add_argument('-sentence_piece_model', default='data/small_1600_en-fr_vocab.model')
    # parser.add_argument('-sentence_piece_vocab', default='data/small_1600_en-fr_vocab.vocab')
    parser.add_argument('-no_cuda', action='store_true')
    parser.add_argument('-SGDR', action='store_true', default=False)
    parser.add_argument('-epochs', type=int, default=500)
    parser.add_argument('-load_weights', default='weights')
    parser.add_argument('-ckpt_file', default='model_weights.ckpt')
    parser.add_argument('-n_layers', type=int, default=3)
    parser.add_argument('-d_vocab', type=int, default=8)
    parser.add_argument('-d_hidden', type=int, default=16)
    parser.add_argument('-heads', type=int, default=4)
    parser.add_argument('-dropout', type=int, default=0.0)
    parser.add_argument('-label_smoothing', type=int, default=0.0)
    parser.add_argument('-batch_size', type=int, default=512)
    parser.add_argument('-printevery', type=int, default=10)
    parser.add_argument('-lr', type=int, default=0.0001)
    # parser.add_argument('-max_tokens', type=int, default=82)
    parser.add_argument('-checkpoint', type=int, default=10)

    args = parser.parse_args()

    args.device = 0 if args.no_cuda is False else -1
    if args.device == 0:
        assert torch.cuda.is_available()
    print('Device :', 'cuda:0' if args.device == 0 else 'cpu')
    print('Batch size :', str(args.batch_size))

    args.train_iterator = process_data(args)
    args.model = get_model(args)

    total_trainable_params = sum(p.numel() for p in args.model.parameters()
                                 if p.requires_grad)
    total_params = sum(p.numel() for p in args.model.parameters())
    print('Number of trainable parameters :', total_trainable_params,
          '/', total_params)

    batch_count = 0
    count = 0
    src_pad_count = 0
    src_tok_count = 0
    trg_pad_count = 0
    trg_tok_count = 0
    for i, (src, trg) in enumerate(args.train_iterator):
        batch_count += 1
        # print('batch n°'+str(batch_count), 'of size', src.size(0))
        for s, t in zip(src, trg):
            # print([args.src_id2t[str(toks)] for toks in s.cpu().numpy().tolist()])
            # print([args.trg_id2t[str(toks)] for toks in t.cpu().numpy().tolist()])
            for toks in s.cpu().numpy().tolist():
                src_tok_count += 1
                if toks == args.pad_id:
                    src_pad_count += 1
            for toks in t.cpu().numpy().tolist():
                trg_tok_count += 1
                if toks == args.pad_id:
                    trg_pad_count += 1
            count += 1
    print('Number of examples iterated :', count)
    print('Batch count :', batch_count)
    print('Src waste (pad tokens) :', str(src_pad_count), '/', str(src_tok_count), '=', str(src_pad_count / src_tok_count * 100), '%')
    print('Trg waste (pad tokens) :', str(trg_pad_count), '/', str(trg_tok_count), '=', str(trg_pad_count / trg_tok_count * 100), '%')

    train_model(args)

if __name__ == "__main__":
    main()
